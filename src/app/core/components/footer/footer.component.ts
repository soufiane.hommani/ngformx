import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-footer',
  standalone: true,
  imports: [CommonModule],
  template: `
    <div class="footer py-3">
      <div class="container">
        <div class="row">
          <div class="col-12 col-md-3 mb-2 mb-md-0">
            <button type="button" class="btn btn-outline-primary w-100">Save</button>
          </div>
          <div class="col-12 col-md-3 mb-2 mb-md-0">
            <button type="button" class="btn btn-outline-secondary w-100">Load</button>
          </div>
          <div class="col-12 col-md-3 mb-2 mb-md-0">
            <button type="button" class="btn btn-outline-success w-100">Export</button>
          </div>
          <div class="col-12 col-md-3">
            <button type="button" class="btn btn-outline-info w-100">Preview</button>
          </div>
        </div>
      </div>
    </div>
  `,
  styles: [`
    .footer {
      border-top: 1px solid #e9ecef;
      background-color: #f8f9fa;
    }
    .btn {
      min-width: 100px;
    }
  `]
})
export class FooterComponent {}
